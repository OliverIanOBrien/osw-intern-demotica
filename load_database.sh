#!/bin/bash

cd ./mongo
rm -r my_mongo
unzip example_house.zip
docker cp my_mongo/ osw-intern-demotica_mongodb-primary_1:/tmp
docker exec osw-intern-demotica_mongodb-primary_1 sh -c 'mongorestore --drop -d my_mongo /tmp/my_mongo'
rm -r my_mongo