import json
from apscheduler.schedulers.background import BackgroundScheduler
from pymongo import MongoClient
from pymongo.errors import ConnectionFailure, PyMongoError
from bson.objectid import ObjectId
from bson.json_util import dumps
from threading import Thread
from flask_socketio import SocketIO

client = None

required_create_data = {
    'light': {
        "dimmable": False,
        "type": '',
        "wattage": 0.0,
        'location': '',
        'height': 0,
        'width': 0
    },
    'room': {
        'name': '',
        'x': '',
        'y': '',
        'width': '',
        'height': ''
    },
    'node': {
        'name': '',
        'location': '',
        'height': '',
        'width': ''
    }

}

update_data = {

    'lights': [
        "active",
        "dimmable",
        "level",
        "type",
        "wattage",
        "last_update",
        "kWh", 'width', 'height'
    ],
    'rooms': [
        'name',
        'x',
        'y',
        'width',
        'height'
    ]
}


def get_mongo_client(url, port):
    client = None
    try:
        client = MongoClient(url, port)
    except ConnectionFailure:
        client = "connection failed"
    return client


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y


class Rect:
    def __init__(self, x, y, w, h):
        self.x1 = x
        self.y1 = y
        self.x2 = x + w
        self.y2 = y + h


def do_overlap(rectA, rectB):
    # if (
    # RectA.X1 < RectB.X2 & &
    # RectA.X2 > RectB.X1 & &
    # RectA.Y1 > RectB.Y2 & &
    # RectA.Y2 < RectB.Y1
    # )
    a = rectA.x1 < rectB.x2
    b = rectA.x2 > rectB.x1
    c = rectA.y1 < rectB.y2
    d = rectA.y2 > rectB.y1
    e = (a and b and c and d)
    return e


def check_overlap(ratios, new_room):
    for ratio in ratios:
        old_room = Rect(
            x=ratio['coordinates']['x'],
            y=ratio['coordinates']['y'],
            w=ratio['size']['width'],
            h=ratio['size']['height']
        )
        overlap = do_overlap(new_room, old_room)
        if overlap:
            print("Rectangles Overlap, with {}".format(ratio['name']))
            return True

    return False


class MongoAdapter:

    def __init__(self, url="mongodb://mongodb-primary:27017,mongodb-secondary:27017,mongodb-arbiter:27017"):
        self.url = url
        try:
            self.client = MongoClient(self.url)
            Thread(target=self.get_kilowatt_statistics, daemon=True).start()
        except:
            print("connection failed")

    def check_client(self):
        return True if self.client else False

    def get_room_by_name(self, room_name):
        client = self.client
        if client:
            database = client.my_mongo
            if database:
                return database.rooms.find_one({"name": room_name})
        return "error"

    def get_rooms_json(self):
        client = self.client
        out = "error"
        if client:
            rooms = client.my_mongo.rooms.find({}, {'_id': 0})
            print(rooms)
            out = []
            for room in rooms:
                out.append(room)

            print(out)

        return dumps(out)

    def get_temperature(self, room_name):
        if room_name in self.client.my_mongo.rooms.distinct('name'):
            room = self.client.my_mongo.rooms.find_one({'name': room_name})
            return {'location': room['name'], 'temperature': room['temperature'], 'unit': room["temperature-unit"]}

    def action_light(self, data):

        #  Extract data
        location = data['location']
        action = data['action']
        level = float(data['level']) if 'level' in data else None

        #  Get collections
        lights_collection = self.client.my_mongo.lights
        room = self.client.my_mongo.rooms.find_one({'name': location})

        # Get Room lights
        lights = lights_collection.find(
            {
                '_id': {
                    '$in': list(map(lambda light_id: ObjectId(light_id), room['lights']))
                }
            }
        )

        # Calculate now in seconds
        import datetime
        now = datetime.datetime.now()
        seconds_since_midnight = (now - now.replace(hour=0, minute=0, second=0, microsecond=0)).total_seconds()

        # Instantiate filter and values dictionaries
        update_filter = {}
        update_values = {}

        if action in ['brighten', 'dim']:
            update_filter['dimmable'] = True
            update_values['level'] = level

        if action in ['on', 'off']:
            update_values['active'] = True if action == 'on' else False

        # Loop over lights
        for light in lights:
            update_filter['_id'] = light['_id']
            update_values['last_update'] = seconds_since_midnight
            # Calculate kWh. maters only if light was on (light['active'] = 1) regardless of brightness
            update_values['kWh'] = self.calculate_kWh(light, seconds_since_midnight)
            self.client.my_mongo.lights.update(
                update_filter,
                {
                    '$set': update_values  # VALUE TO UPDATE
                }
            )

        # for light in list(self.client.my_mongo.lights.find({'_id': {'$in': lights}})):
        #     kWh = light['kWh']
        #     last_action_time = light["last_action_time"]
        #     if light['active']:
        #         active_hours = (seconds_since_midnight-last_action_time)/3600
        #         last_action_time = seconds_since_midnight
        #         brightness_multiplier = light['level']/100 if light['dimmable'] else 1
        #         kWh += light['wattage']*brightness_multiplier*active_hours
        #
        #         self.client.my_mongo.lights.update(
        #             {
        #
        #             },
        #             {
        #
        #             }
        #         )
        #

        return {'response': 'success'}

    def set_temperature(self, data):
        location = data['location']
        temperature = data['temperature']
        self.client.my_mongo.rooms.update({'name': location}, {'$set': {'temperature': float(temperature)}})
        return {'response': 'success'}

    def calculate_kWh(self, light, current_time):
        hours_since_update = (current_time - light['last_update']) / 3600
        kW = light['wattage'] / 1000 * light['level'] / 100
        kWh = kW * hours_since_update * light['active']
        return light['kWh'] + kWh

    def get_kilowatt_statistics(self):  # THREAD
        statistics = self.client.my_mongo.house_statistics
        lights = self.client.my_mongo.lights
        from _datetime import datetime
        last_update_time = datetime.now().timestamp()
        try:
            with self.client.my_mongo.lights.watch(
                    [{'$match': {'operationType': 'update'}}],  # Pipeline
                    full_document='updateLookup'
            ) as stream:

                for change in stream:
                    update_time = datetime.now().timestamp()
                    if last_update_time + 1 <= update_time:
                        last_update_time = update_time
                        active_lights = list(lights.find({'active': True}, {'_id': 0, 'type': 0, 'active': 0}))
                        total_watts = sum(
                            map(
                                lambda light:
                                light['wattage'] * light['level'] / 100 if light['dimmable'] else light['wattage'],
                                active_lights
                            )
                        )

                        statistics.update(
                            {},
                            {
                                '$set': {
                                    'total_watts': total_watts
                                }
                            }
                        )

        except PyMongoError:
            print(PyMongoError)

    def hourly_statistics(self):
        print("here")
        light_collection = self.client.my_mongo.lights
        room_collection = self.client.my_mongo.rooms
        rooms = {
            room['name']: room['lights']
            for room in list(room_collection.find())
        }
        lights = {
            str(light['_id']):
                {
                    'used': light['kWh'] + self.calculate_kWh(light, 24 * 60 * 60)
                } for light in list(light_collection.find())}
        for key in rooms.keys():
            for room_lights in rooms[key]:
                lights[room_lights]['location'] = key
        consumption = sum(light['used'] for light in lights.values())

        import datetime
        date = str(datetime.datetime.today().date())

        today = {
            'kWh': consumption,
            'lights': lights
        }

        statistics_collection = self.client.my_mongo.house_statistics
        daily_consumption = statistics_collection.find_one()['daily_consumption']
        daily_consumption[date] = today
        statistics_collection.update(
            {}, {'$set': {'daily_consumption': daily_consumption}}
        )

        light_collection.update_many(
            {},
            {'$set': {
                'kWh': 0.0,
                'last_update': 0.0
            }}
        )

    def get_house(self):
        database = self.client.my_mongo
        if database:
            rooms = database.rooms
            lights = database.lights
            nodes = database.nodes
            data = {
                'rooms': {},
                'lights': {},
                'nodes': {},
            }

            for light in list(lights.find({})):
                light['id'] = str(light['_id'])
                del light['_id']
                data['lights'][light['id']] = light

            for node in list(nodes.find({})):
                node['id'] = str(node['_id'])
                del node['_id']
                data['nodes'][node['id']] = node

            for room in list(rooms.find({})):
                room['id'] = str(room['_id'])
                del room['_id']
                room['lights'] = list(map(lambda light_id: data['lights'][light_id], room['lights']))
                room['nodes'] = list(map(lambda node_id: data['nodes'][node_id], room['nodes']))
                data['rooms'][room['id']] = room

            return data
        return {'error': 'error'}

    def check_data(self, data, type):
        error_message = {
            'error': 'Data is missing, empty or invalid in the create query',
            'type': [],
            'values': {}
        }
        req_data = required_create_data[type]

        if any(key not in data for key in req_data):
            error_message['type'].append('missing')
            error_message['values']['missing'] = list(filter(lambda key: key not in data, req_data))

        if any(str(data[key]).strip() == '' for key in req_data):
            error_message['type'].append('empty')
            error_message['values']['empty'] = list(filter(lambda key: str(data[key]).strip() == '', req_data))

        return error_message

    def add_light(self, data):
        data = dict(data)
        data_error, error_message = self.check_light(data)
        if data_error:
            return error_message
        else:
            error_message['error'] = 'Oops, something went wrong on our part, we will look at this as soon as possible.' \
                                     ' Our apologies for the inconvenience. '

            if self.check_client():
                try:
                    lights = self.client.my_mongo.lights

                    insert_data = {
                        "active": False,
                        "dimmable": data['dimmable'],
                        "level": 100.0,
                        "type": data["type"],
                        "wattage": data['wattage'],
                        "last_update": 0.0,
                        "kWh": 0.0,
                        'width': data['width'],
                        'height': data['height'],
                        'location': data['location']
                    }

                    id = lights.insert_one(insert_data).inserted_id

                    rooms = self.client.my_mongo.rooms
                    location = data['location']
                    room_lights = rooms.find_one(
                        {'name': location}
                    )
                    room_lights = room_lights['lights']
                    room_lights.append(str(id))

                    rooms.update_one({'name': location}, {'$set': {'lights': room_lights}})

                    return {'success': 'Light added'}

                except TypeError as e:
                    error_message['type'].append('Server')
                    error_message['values']['Server'] = [str(e)]

            return error_message

    def check_light(self, data):
        error_message = self.check_data(data, 'light')
        if len(error_message['type']) == 0:
            try:
                data['width'] = int(data['width'])
                data['height'] = int(data['height'])
            except TypeError or ValueError as e:
                error_message['type'].append('type')
                error_message['values']['type'] = ['{} {}'.format('Width or Height', str(e))]
            try:
                data['wattage'] = float(data['wattage'])
            except TypeError or ValueError as e:
                error_message['type'].append('type')
                error_message['values']['type'] = ['{} {}'.format('Wattage', str(e))]
            try:
                data['dimmable'] = bool(data['dimmable'])
            except TypeError or ValueError as e:
                error_message['type'].append('type')
                error_message['values']['type'] = ['{} {}'.format('Dimmable', str(e))]

            if self.client.my_mongo.rooms.count_documents({'name': data['location']}) == 0:
                error_message['type'].append('room')
                error_message['values']['room'] = ['Location {} not found'.format(data['location'])]

        return (len(error_message['type']) > 0), error_message

    def add_room(self, data):
        data = dict(data)
        error, error_message = self.check_room(data)
        if error:
            return error_message
        if self.check_client():

            insert_data = {
                'name': data['name'],
                'temperature': 20.0,
                'temperature-unit': 'C',
                'lights': [],
                'coordinates': {
                    'x': int(data['x']),
                    'y': int(data['y'])
                },
                'size': {
                    'width': int(data['width']),
                    'height': int(data['height'])
                }
            }
            rooms = self.client.my_mongo.rooms
            try:
                rooms.insert_one(insert_data)
                return {'success': 'Room added'}
            except TypeError as e:
                error_message['type'].append('Server')
                error_message['values']['Server'] = [str(e)]

        return error_message

    def check_room(self, data):
        error_message = self.check_data(data, 'room')
        if len(error_message['type']) == 0:
            acceptable_names = self.client.my_mongo.entities_values.find_one(
                {'collection': 'rooms'},
                {'values': 1, '_id': 0}
            )['values']
            if data['name'] not in acceptable_names:
                error_message['type'].append('Invalid')
                error_message['values']['Invalid'] = ['Location \'{}\' not acceptable'.format(data['name'])]

            if self.client.my_mongo.rooms.count_documents({'name': data['name']}) > 0:
                error_message['type'].append('Exists')
                error_message['values']['Exists'] = ['Location {} already exists'.format(data['name'])]

            new_room = Rect(
                x=int(data['x']),
                y=int(data['y']),
                w=int(data['width']),
                h=int(data['height'])
            )

            ratios = list(self.client.my_mongo.rooms.find({}, {'coordinates': 1, 'size': 1, 'name': 1}))

            if check_overlap(ratios, new_room):
                error_message['type'].append('Overlap')
                error_message['values']['Overlap'] = ['Room overlaps another']

        return (len(error_message['type']) > 0), error_message

    def delete_light(self, data):
        id = data['id']
        lights = self.client.my_mongo.lights
        if lights:
            try:
                lights.delete_one({'_id': ObjectId(id)})
            except PyMongoError as e:
                print(e)
                return e
        return "success"

    def delete_room(self, data):
        id = data['id']
        rooms = self.client.my_mongo.rooms
        if rooms:
            try:
                rooms.delete_one({'_id': ObjectId(id)})
            except PyMongoError as e:
                print(e)
                return e
        return "success"
        pass

    def get_available_rooms(self):
        db = self.client.my_mongo
        if db:
            room_values = db.entities_values.find_one({'collection': 'rooms'}, {'values': 1, '_id': 0})['values']
            rooms = list(room['name'] for room in db.rooms.find({}))
            room_values = list(filter(lambda room: room not in rooms, room_values))
            return {'rooms': room_values}
        return 'fail'

    def update_light(self, data):
        data = dict(data)
        if 'id' not in data:
            return {'error': 'Id required'}

        id = data['id']
        del data['id']
        lights = self.client.my_mongo.lights
        ids = lights.find({}, {'_id': 1})
        if id not in map(lambda _id: str(_id['_id']), list(ids)):
            return {'error': 'Id invalid'}

        insert = update_data['lights']
        if any(key not in insert for key in data.keys()):
            return {'error': 'Not all keys are valid'}

        if 'active' in data:
            data['active'] = True if data['active'] == 'True' else False

        if 'dimmable' in data:
            data['dimmable'] = True if data['dimmable'] == 'True' else False
            if not data['dimmable']:
                data['level'] = 100.0
            elif 'level' in data:
                data['level'] = float(data['level'])

        if 'width' in data:
            data['width'] = float(data['width'])

        if 'height' in data:
            data['height'] = float(data['height'])

        if 'wattage' in data:
            data['wattage'] = float(data['wattage'])

        if lights:
            lights.update_one(
                {'_id': ObjectId(id)},
                {'$set': data}
            )

        return {'success': data}

    def update_room(self, data):
        data = dict(data)
        if 'id' not in data:
            return {'error': 'id required'}
        id = data['id']
        del data['id']
        rooms = self.client.my_mongo.rooms
        if id not in map(lambda _id: str(_id['_id']), list(rooms.find({}, {'_id': 1}))):
            return {'error': 'id invalid'}

        insert = update_data['rooms']
        if any(key not in insert for key in data.keys()):
            return {'error': 'One or many keys are invalid'}

        room = rooms.find_one({'_id': ObjectId(id)})

        if any(key in data for key in ['x', 'y', 'width', 'height']):
            x = int(data['x']) if 'x' in data else room['coordinates']['x']
            y = int(data['y']) if 'y' in data else room['coordinates']['y']
            width = int(data['width']) if 'width' in data else room['size']['width']
            height = int(data['height']) if 'height' in data else room['size']['height']

            rect = Rect(x, y, width, height)

            ratios = list(self.client.my_mongo.rooms.find({'_id': {'$nin': [ObjectId(id)]}},
                                                          {'coordinates': 1, 'size': 1, 'name': 1}))
            if check_overlap(ratios, rect):
                return {'error': 'room overlaps with another'}
            data['coordinates'] = {
                'x': x, 'y': y
            }
            data['size'] = {
                'width': width, 'height': height
            }

            for key in ['x', 'y', 'width', 'height']:
                if key in data: del data[key]

        if 'name' in data:
            names = list(self.get_available_rooms()['rooms'])
            names.append(room['name'])
            print(names)
            if data['name'] not in names:
                return {'error': 'Name is not permitted'}

        print(data)
        rooms.update_one({'_id': ObjectId(id)}, {'$set': data})
        return {'success': data}

    def add_node(self, data):
        data = dict(data)
        error, error_message = self.check_node(data)
        if error:
            return error_message
        node_data = {
            'location': data['location'],
            'name': data['name'],
            'active': False,
            'coordinates': {
                'width': data['width'],
                'height': data['height']
            }
        }
        nodes = self.client.my_mongo.nodes
        rooms = self.client.my_mongo.rooms
        try:
            id = nodes.insert_one(node_data).inserted_id
            room_nodes = list(rooms.find_one({'name': data['location']}, {'nodes': 1})['nodes'])
            room_nodes.append(str(id))
            rooms.update_one({'name': data['location']}, {'$set': {'nodes': room_nodes}})
        except TypeError as e:
            error_message['type'].append('Server')
            error_message['values']['Server'] = [str(e)]
        return error_message if len(error_message['type']) > 0 else {'success': 'node added'}

    def check_node(self, data):
        error_message = self.check_data(data, 'node')
        if len(error_message['type']) == 0:

            values = self.client.my_mongo.entities_values.find_one({'collection': 'nodes'})['values']
            if data['name'] not in values:
                error_message['type'].append('invalid')
                error_message['values']['invalid'] = 'Name {} is invalid, use {}'.format(data['name'], values)

            rooms = list(map(
                lambda room: room['name'],
                self.client.my_mongo.rooms.find()
            ))

            if data['location'] not in rooms:
                error_message['type'].append('invalid')
                error_message['values']['invalid'] = 'Name {} is invalid or does not exist, use {}'.format(
                    data['location'], rooms)

            try:
                data['width'] = int(data['width'])
                data['height'] = int(data['height'])
            except TypeError as e:
                error_message['type'].append('type')
                error_message['values']['type'] = '{} {}'.format('Width or height', str(e))

        return (len(error_message['type']) > 0), error_message

    def update_node(self, data):
        data = dict(data)
        name = data['name']
        location = data['location']
        state = True if (data['action'] == 'on') else False

        room = self.client.my_mongo.rooms.find_one({'name': location})
        nodes = self.client.my_mongo.nodes

        room_nodes_names = list(
            map(
                lambda _id: nodes.find_one({'_id': ObjectId(_id)})['name'],
                room['nodes']
            )
        )

        if name not in room_nodes_names:
            return {'error': {
                'type': ['unavailable'],
                'message': '{} not found in the {}'.format(name, location)
                }
            }

        self.client.my_mongo.nodes.update_one(
            {'name': name, 'location': location},
            {'$set': {'active': state}}
        )
        return {'success': True}
        pass

    if __name__ == '__main__':
        pass


#       Watcher class that takes the Mongo adaptor and checks for updates
#
#


class Watcher:
    def __init__(self, adaptor, socket):
        if isinstance(adaptor, MongoAdapter):
            self.mongo = adaptor
        else:
            raise TypeError('MongoAdaptor Required')
        if isinstance(socket, SocketIO):
            self.socket = socket
        else:
            raise TypeError('SocketIO Required')

        # Start the threads
        try:
            Thread(target=self.watch_light_update, daemon=True).start()
            Thread(target=self.watch_room_update, daemon=True).start()
            Thread(target=self.watch_light_create, daemon=True).start()
            Thread(target=self.watch_node_update, daemon=True).start()

            daemon = BackgroundScheduler()
            daemon.add_job(self.hourly_statistics(), 'interval', minutes=1)
            daemon.add_job(self.tick, 'interval', seconds=5)
            daemon.start()
        except RuntimeError as e:
            print(e)

    def watch_light_update(self):
        try:
            with self.mongo.client.my_mongo.lights.watch(
                    [{'$match': {'operationType': 'update'}}],  # Pipeline
                    full_document='updateLookup'
            ) as stream:
                for change in stream:
                    update = {
                        'id': str(change['documentKey']['_id']),
                        'values': change['updateDescription']['updatedFields']
                    }
                    print(update)
                    self.emit('light-change', update)
        except PyMongoError:
            print(str(PyMongoError) + 'bla')

    def watch_room_update(self):
        try:
            with self.mongo.client.my_mongo.rooms.watch(
                    [{'$match': {'operationType': 'update'}}],  # Pipeline
                    full_document='updateLookup'
            ) as stream:
                for change in stream:
                    update = {
                        'id': str(change['documentKey']['_id']),
                        'values': change['updateDescription']['updatedFields']
                    }
                    print(update)
                    self.emit('room-change', update)
        except PyMongoError:
            print(str(PyMongoError) + 'bla')

    def watch_light_create(self):
        try:
            with self.mongo.client.my_mongo.lights.watch(
                    [{'$match': {'operationType': 'insert'}}]
            ) as stream:
                for change in stream:
                    new_light = change['fullDocument']
                    new_light['id'] = str(new_light['_id'])
                    del new_light['_id']
                    data = {
                        'id': new_light['id'],
                        'values': new_light
                    }
                    self.emit('light-create', data)
                    # update = {
                    #     'id': str(change['documentKey']['_id']),
                    #     'values': change['updateDescription']['updatedFields']
                    # }
                    # print(update)
                    # self.emit('light-create', update)
        except PyMongoError:
            print(str(PyMongoError) + 'bla')

    def watch_node_create(self):
        try:
            with self.mongo.client.my_mongo.nodes.watch(
                    [{'$match': {'operationType': 'insert'}}]
            ) as stream:
                for change in stream:
                    new_node = change['fullDocument']
                    new_node['id'] = str(new_node['_id'])
                    del new_node['_id']
                    data = {
                        'id': new_node['id'],
                        'values': new_node
                    }
                    self.emit('node-create', data)
        except PyMongoError:
            print(str(PyMongoError) + 'bla')

    def watch_node_update(self):
        try:
            with self.mongo.client.my_mongo.nodes.watch(
                    [{'$match': {'operationType': 'update'}}],  # Pipeline
                    full_document='updateLookup'
            ) as stream:
                for change in stream:
                    update = {
                        'id': str(change['documentKey']['_id']),
                        'values': change['updateDescription']['updatedFields']
                    }
                    print(update)
                    self.emit('node-change', update)
        except PyMongoError:
            print(str(PyMongoError) + 'bla')

    def tick(self):
        self.socket.emit('tick', 'I am here')

    def hourly_statistics(self):
        self.mongo.hourly_statistics()

    def emit(self, message, data):
        self.socket.emit(message, data)
