from apscheduler.schedulers.background import BackgroundScheduler
from flask import Flask, request
from flask_socketio import SocketIO
from flask_cors import CORS
from mongo_scripts import MongoAdapter, Watcher


app = Flask(__name__)
CORS(app)
url = 'localhost:27017'
daemon = BackgroundScheduler()
socket = SocketIO(app, cors_allowed_origins="*")
mongo = MongoAdapter()
watcher = Watcher(mongo, socket)

# CRUD
@app.route('/<action>', methods=["GET", "POST"])
def check_action(action):
    data = request.form
    # Create
    if action == 'createLight':
        return mongo.add_light(data)
    if action == 'createRoom':
        return mongo.add_room(data)
    if action == 'createNode':
        return mongo.add_node(data)

    # Read
    elif action == "getTemperature" and "location" in data:
        return mongo.get_temperature(data["location"])
    elif action == "getRooms":
        return mongo.get_rooms_json()
    elif action == 'getRoom':
        return mongo.get_room_by_name(data['location'])
    elif action == 'getHouse':
        return mongo.get_house()
    elif action == 'hello':
        return {'message': 'hello, Dave!'}
    elif action == 'getAvailableRooms':
        return mongo.get_available_rooms()

    # Update
    elif action == 'setTemperature':
        return mongo.set_temperature(data)
    elif action == 'lightAction':
        return mongo.action_light(data)
    elif action == 'lightUpdate':
        return mongo.update_light(data)
    elif action == 'roomUpdate':
        return mongo.update_room(data)
    elif action == 'updateNode':
        return mongo.update_node(data)


    # Delete
    elif action == 'deleteLight':
        return mongo.delete_light(data)

    elif action == 'deleteRoom':
        return mongo.delete_room(data)


    return {'Error': 'No action found'}


# Update

# {
#     "_id" : ObjectId("5e7dff52927e844368bd672c"),
#     "active" : true,
#     "dimmable" : true,
#     "level" : 50.0,
#     "type" : "LED",
#     "wattage" : 7.0,
#     "last_update" : 60630.318283,
#     "kWh" : -0.213639715763333,
#     "height" : "50.0",
#     "width" : 50.0
# }


# Delete


if __name__ == '__main__':
    socket.run(app, host='0.0.0.0', debug=True, port=5000)
