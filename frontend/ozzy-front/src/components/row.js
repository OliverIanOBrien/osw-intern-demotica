import React from "react";
import Grid from '@material-ui/core/Grid'
import Cell from './cell'

class Row extends React.Component{
    render() {
        let cells = []
        for (let i = 0; i < 10; i++) cells.push(i)
        return(
            <Grid>
                {cells.map(value => (
                    <Cell/>
                ))}
            </Grid>
        )
    }
}

export default Row