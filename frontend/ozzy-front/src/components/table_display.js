import React from "react";
import {TableHouse} from "./house";
import table from "../images/table.png";
import Header from "./header";
import './table.css'
import {TableLight} from "./light";

class TableDisplay extends React.Component{
    constructor(props) {
        super(props);
    }
    render(){
        return(
            <div id='table'>
                <Header icon={table} title='House table' id='table_header'/>
                <TableHouse state={this.props.state}/>
            </div>
        )
    }
}

export default TableDisplay;