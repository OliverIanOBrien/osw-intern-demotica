import React from "react";
import './node.css'

class TableNode extends React.Component{

    render() {
        const {id, name, location, active, coordinates} = this.props.data;
        const {width, height} = coordinates;

        const filters = this.props.filters;

        let cells = [];

        if(filters.name === true) cells.push(<td>{name}</td>)
        if(filters.location === true) cells.push(<td>{location}</td>)
        if(filters.active === true) cells.push(<td>{(active === true)? 'On' : 'Off'}</td>)
        if(filters.coordinates === true) cells.push(<td>{width+'% '+height+'%'}</td>)
        if(filters.id === true) cells.push(<td>{id}</td>)

        return (
            <tr key={id} id={id}>
                {cells}
            </tr>
        )
    }

}

class FloorPlanNode extends React.Component {

    constructor(props) {
        super(props);

    }

    render() {
        const {id, name, coordinates, active} = this.props.node;
        const {width, height} = coordinates;
        const color = (active === true) ? 'lightgreen' : 'red';
        const container = {
            position: 'absolute',
            top: height+'%', left: width+'%',
        };
        const style = {
            position: 'absolute',
            margin: '10px',
            width: '25px', height: '25px',
            borderRadius: '5px',
            backgroundColor: color,
            border: 'grey solid 2px',
            boxShadow: '0px 0px 10px 5px '+color,
            transition: 'background 1s ease-in, box-shadow 0.8s ease-in-out',
            zIndex: 3
        };
        return(
            <div className={'node_container'} style={container}>
                <p style={style}/>
                <div className={'node_name'}>{name}</div>
            </div>
        )
    }

}

export {TableNode, FloorPlanNode}