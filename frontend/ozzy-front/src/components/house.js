import React, {useEffect} from 'react';
import {TableLight, FloorPlanLight} from "./light";
import {TableRoom, FloorPlanRoom} from "./room";
import io from 'socket.io-client';
import './house.css'
import table from "../images/table.png";
import Header from "./header";
import {Table, FilterableTable} from "./table";

const socket = io('')

class House extends React.Component {

    extract_values(dictionary) {
        return Object.keys(dictionary).map(function (key) {
            return dictionary[key];
        });
    }
}

class TableHouse extends House {

    render() {
        let {lights, rooms, nodes} = this.props.state;

        const room_options = {
            filters: {
                id: false,
                name: true,
                temperature: true,
                lights: true,
                size: true,
                coordinates: false,
            }
        };
        const light_options = {
            filters: {
                location: true,
                active: true,
                dimmable: true,
                level: true,
                type: true,
                wattage: true,
                kWh: true,
                id: true,
            }
        };
        const node_option = {
            filters: {
                name: true,
                location: true,
                active: true,
                coordinates: true,
                id: false,
            }
        };

        const room_table = (Object.keys(rooms).length !== 0) ?
            <Table name='Rooms' rows={rooms} options={room_options}/> :
            <div/>;
        const light_table = (Object.keys(lights).length !== 0) ?
            <Table name='Lights' rows={lights} options={light_options}/> :
            <div/>;

        const nodes_table = (Object.keys(nodes).length !== 0) ?
            <Table name={'Nodes'} rows={nodes} options={node_option}/> :
            <div/>;

        return (
            <div id='house'>
                <div id='house_table'>
                    {room_table}
                    {light_table}
                    {nodes_table}
                </div>
            </div>
        )
    }
}


class FloorPlanHouse extends House {

    render() {
        let {lights, rooms, nodes} = this.props.state;
        const room_display = (Object.values(rooms).length > 0) ?
            super.extract_values(rooms).map((value) => {
                    const light_ids = Object.values(value['lights']).map(l => l['id']);
                    const room_lights = Object.values(lights).filter(light => {
                        return (light_ids.includes(light['id']))
                    });
                    const node_ids = Object.values(value['nodes']).map(n => n['id']);
                    const room_nodes = Object.values(nodes).filter((node) => {
                        return (node_ids.includes(node['id']))
                    });
                    return (<FloorPlanRoom
                        room={value}
                        temperature={this.props.filters.temperature}
                        display_lights={this.props.filters.lights}
                        lights={room_lights}
                        display_nodes={this.props.filters.nodes}
                        nodes={room_nodes}
                    />)
                }
            ) : (<div/>);
        return (
            <div id='blueprint'>
                {room_display}
            </div>
        )
    }
}

export {TableHouse, FloorPlanHouse}