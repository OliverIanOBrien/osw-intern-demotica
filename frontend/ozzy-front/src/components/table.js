import React from "react";
import table from "../images/table.png";
import {TableLight} from "./light";
import {TableRoom} from "./room";
import {TableNode} from "./node";

class Table extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            display: true,
            options: {},
        }
    }

    render() {
        const name = this.props.name;
        const row_data = this.props.rows;
        const filters = this.props.options.filters;

        let name_el = (<div/>);
        if (name !== undefined) {
            name_el = (
                <div className='table_title_container' onClick={() => this.setState({display: !this.state.display})}>
                    <div className='table_title'>{name}</div>
                </div>
            )
        }

        let rows;
        switch (this.props.name) {
            case 'Lights':
                rows = Object.values(this.props.rows).map(
                    light => <TableLight data={light} filters={filters}/>
                );
                break;
            case 'Rooms':
                rows = Object.values(this.props.rows).map(
                    room => (<TableRoom data={room} filters={filters}/>)
                );
                break;
            case 'Nodes':
                rows = Object.values(this.props.rows).map(
                    node => (<TableNode data={node} filters={filters}/>)
                );
                break;

        }
        const headers = Object.keys(Object.values(row_data)[0])
            .filter((key) => Object.keys(filters).includes(key))
            .filter((key) => filters[key])
            .sort((k1,k2) => {
                return Object.keys(filters).indexOf(k1) - Object.keys(filters).indexOf(k2)
            })
            .map(key => <td>{key.toString()}</td>)


        return (
            <div className='table_container'>
                {name_el}
                <div className={(this.state.display) ? 'table_open' : 'table_closed'}>
                    <table>
                        <thead className='table_head'>
                        <tr>
                            {headers}
                        </tr>
                        </thead>
                        <tbody>
                        {rows}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }

}

export {Table};