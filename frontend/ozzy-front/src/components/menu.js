import React from "react";
import './menu.css';
import floor_plan from '../images/floor_plan.png'
import table from "../images/table.png";
import icon from '../images/Ozzy_small.png'
import open from '../images/open.png';

class Menu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: true,
            over: false
        }
        setTimeout(this.auto_close, 5000)
    }

    scroll(ref) {
        const tab = document.getElementById(ref)
        tab.scrollIntoView({behavior: 'smooth'})
    }



    auto_close = () => {
        if(this.state.over !== true) {this.setState({open: false})}
        else if (this.state.open === true) {setTimeout(this.auto_close,5000)}
    };


    toggle_menu = () => {
        this.setState({open: !this.state.open});
        setTimeout(this.auto_close, 5000)
    };

    mouse_over = (isOver) => {
        this.setState({over: isOver})
    };

    render() {
        return (
            <div id='menu_container' className={(this.state.open === true) ? 'open_menu' : 'closed_menu'}
                onMouseOver={() => this.mouse_over(true)}
                onMouseOut={() => this.mouse_over(false)}
            >
                <div className='menu_title'
                    onClick={() => this.toggle_menu()}>
                    <h3>Menu</h3>
                    <img src={open} alt='null'/>
                </div>
                <div id='main_menu'>
                    <div className='menu_tab'
                        onClick={
                            () => this.scroll('home_header')
                        }>
                        <h3> Home </h3>
                        <img src={icon} alt='temp'/>
                    </div>
                    <div className='menu_tab'
                        onClick={
                            () => this.scroll('table_header')
                        }>
                        <h3> House Data </h3>
                        <img src={table} alt='temp'/>
                    </div>
                    <div className='menu_tab'
                        onClick={
                            () => this.scroll('floor_header')
                        }>
                        <h3> Floor Plan </h3>
                        <img src={floor_plan} alt='temp'/>
                    </div>
                </div>
            </div>
        )
    }
}

export default Menu;