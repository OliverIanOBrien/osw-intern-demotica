import React from "react";

class TableLight extends React.Component {

    render() {

        const {id, active, dimmable, level, type, wattage, kWh, location} = this.props.data;
        const filters = this.props.filters;

        const table_cells = [];

        if (filters.location) table_cells.push(<td>{location}</td>);
        if (filters.active) table_cells.push(<td>{active ? 'On' : 'Off'}</td>);
        if (filters.dimmable) table_cells.push(<td>{dimmable ? 'Dimmable' : 'Static'}</td>);
        if (filters.level) table_cells.push(<td>{dimmable ? level + '%' : 'N/A'}</td>);
        if (filters.type) table_cells.push(<td>{type}</td>);
        if (filters.wattage) table_cells.push(<td>{wattage} W</td>);

        const kiloWatthour = Number(kWh.toFixed(4));
        if (filters.kWh) table_cells.push(<td>{kiloWatthour} kWh</td>);

        if (filters.id) table_cells.push(<td>{id}</td>);

        return (
            <tr key={id} id={id} className='table_row'>
                {table_cells}
            </tr>
        )

    }
}

class FloorPlanLight extends React.Component {

    render() {
        const {id, active, dimmable, level, type, wattage, kWh, location, width, height} = this.props.light;
        const active_multiplyer = (active === true) ? 1 : 0;
        const brightness = level * active_multiplyer / 100;
        const style = {
            position: 'absolute',
            width: '25px',
            height: '25px',
            top: height + '%',
            left: width + '%',
            borderRadius: '50%',
            margin: '-12.5px',
            backgroundColor: 'rgb(255, 255, 0,' + brightness + ')',
            transition: 'background 1s ease-in, box-shadow 0.8s ease-in-out',
            border: 'rgba(154,154,154) solid 1px',
            boxShadow: '0px 0px 30px 20px rgb(255, 255, 0,' + brightness+ ')',
            zIndex: 3
        }
        return (
            <p style={style}></p>
        )
    }
}


export {TableLight, FloorPlanLight};