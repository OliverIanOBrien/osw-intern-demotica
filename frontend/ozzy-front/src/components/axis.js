import React from "react";

class Axis extends React.Component{

    render(){
        let range = [];
        if(this.props.orientation === 'y'){
            for (let i = 0; i < 30; i+=1){range.push(i)}
            range = range.splice(0,60)
            let style = {
                left: 0,
                top: 0,
                width: '5px',
                height: '1200px',
                borderLeft: 'white',
                borderWidth: 2,
                backgroundColor: 'white',
                position: 'sticky',
                zIndex: 1
            };


            return(
                <div style={style}>
                    {range.map(i => {
                        let num = !(i%2) ? i : '';
                        let number_style = {
                                color: 'white',
                                position: 'absolute',
                                paddingLeft: '10px',
                                top: i * 40+'px',
                                width: 'auto',
                                height: 'auto',
                                borderTop: '2px solid white',
                            };

                        return(<div style={number_style}>{num}</div>
                    )
                    })}
                </div>
            )
        }
        if(this.props.orientation === 'x'){
            for (let i = 0; i < 50; i+=1){range.push(i)}
            range = range.splice(0,60)
            let style = {
                right: 0,
                top: 0,
                width: '2000px',
                height: '5px',
                borderLeft: 'white',
                borderWidth: 2,
                backgroundColor: 'white',
                position: 'sticky',
                zIndex: 1
            };


            return(
                <div style={style}>
                    {range.map(i => {
                        let num = (i%2) ? i : '';
                        let number_style = {
                                color: 'white',
                                position: 'absolute',
                                paddingTop: '7px',
                                left: i * 40+'px',
                                width: 'auto',
                                height: 'auto',
                                borderLeft: '2px solid white',
                            };

                        return(<div style={number_style}>{num}</div>
                    )
                    })}
                </div>
            )
        }
    }
}

export default Axis