import React from "react";

class Header extends React.Component {
    render() {
        const title = this.props.title;
        const icon = this.props.icon;
        const id = this.props.id;
        const options = (this.props.options !== undefined)? this.props.options : (<div/>);
        return (
            <div id={id} className='tab_menu'>
                <div className='left'>
                    <img src={icon} className='icon'/>
                    <h3 className='tab_title'>{title}</h3>
                </div>
                <div className='right'>
                    {options}
                </div>
            </div>
        )
    }
}

export default Header