import React from 'react';
import './blue_print.css'
import {TableHouse, FloorPlanHouse} from "./house";
import Axis from "./axis";
import Header from "./header";

import floor_plan from '../images/floor_plan.png'
import temperature_digits from '../images/temperature_digits.png'
import temperature_color from '../images/temperature_color.png'
import open from '../images/open.png'

const temperatures = [
    '#ff4b4b', '#ff6f3b', '#ff922d', '#ffb326', '#ffd32f', '#ffd855', '#ffdc74', '#ffe190', '#ffdbba', '#ffe3e8', '#fff2ff', '#ffffff',
    '#ffffff', '#fbfbff', '#f4f8ff', '#ecf5ff', '#e3f3ff', '#d8f2ff', '#ccf2ff', '#c0f2ff', '#aef3ff', '#9bf5ff', '#87f6fd', '#71f7f9',
];

class BluePrint extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            temperature: false,
            lights: true,
            nodes: true,
            filters: {
                light: {
                    id_filter: true,
                    active_filter: true,
                    dimmable_filter: true,
                    level_filter: true,
                    type_filter: true,
                    wattage_filter: true,
                    kWh_filter: true,
                    location_filter: true
                },
            }
        }
    }

    render() {
        let cells = [];
        for (let i = 0; i < 10; i++) cells.push(i);
        const options = (
                    <div id='floor_plan_options'>

                        <div
                            className='floor_plan_options_button '
                            onClick={
                                () =>
                                    this.setState(
                                        {temperature: !this.state.temperature}
                                    )
                            }
                            title='Toggle heating display'>
                            <div className='option_title'>&deg;C color</div>
                            <div className={(this.state.temperature)? 'enabled' : ''}>
                                <div className='option_slider'>
                                    <div className='option_nub'/>
                                </div>
                            </div>
                        </div>

                        <div
                            className='floor_plan_options_button '
                            onClick={
                                () =>
                                    this.setState(
                                        {lights: !this.state.lights}
                                    )
                            }
                            title='Toggle light display'>
                            <div className='option_title'>Lights Display</div>
                            <div className={(this.state.lights)? 'enabled' : ''}>
                                <div className='option_slider'>
                                    <div className='option_nub'/>
                                </div>
                            </div>
                        </div>

                        <div
                            className='floor_plan_options_button '
                            onClick={
                                () =>
                                    this.setState(
                                        {nodes: !this.state.nodes}
                                    )
                            }
                            title='Toggle light display'>
                            <div className='option_title'>Nodes Display</div>
                            <div className={(this.state.nodes)? 'enabled' : ''}>
                                <div className='option_slider'>
                                    <div className='option_nub'/>
                                </div>
                            </div>
                        </div>

                    </div>
        );
        const temp_bar ={
            background: `linear-gradient(to left,`+temperatures+`)`,

        }
        return (
            <div id='Container'>
                <Header icon={floor_plan} title='Floor Plan' id='floor_header' options={options}/>

                <div className='floor_plan_container'>

                    <div className='floor_plan'>

                        <div className='axis'>
                            <Axis orientation='y'/>
                            <Axis orientation='x'/>
                        </div>

                        <FloorPlanHouse state={this.props.state} filters={this.state}/>

                        <div id='floor_plan_info'>
                            <div id='floor_plan_ratio'>
                                <div id='floor_plan_ratio_square'/>
                                <div id='floor_plan_ratio_index'>1m x 1m</div>
                            </div>
                            <div id='temperature_bar' className={(this.state.temperature)? 'visible': ''}>
                                <ul>
                                    <li className='first'>10&deg;</li>
                                    <li>20&deg;</li>
                                    <li className='last'>30&deg;</li>
                                </ul>
                                <div style={temp_bar} className='temp_colors'/>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        )
    }
}

export default BluePrint;