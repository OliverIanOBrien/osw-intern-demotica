import React from "react";
import './room.css'
import createBreakpoints from "@material-ui/core/styles/createBreakpoints";
import table from "../images/table.png";
import {TableLight, FloorPlanLight} from "./light";
import {Table} from "./table";
import {FloorPlanNode} from "./node";

const temperatures = [
    '#ff4b4b', '#ff6f3b', '#ff922d', '#ffb326', '#ffd32f', '#ffd855', '#ffdc74', '#ffe190', '#ffdbba', '#ffe3e8', '#fff2ff', '#ffffff',
    '#ffffff', '#fbfbff', '#f4f8ff', '#ecf5ff', '#e3f3ff', '#d8f2ff', '#ccf2ff', '#c0f2ff', '#aef3ff', '#9bf5ff', '#87f6fd', '#71f7f9',
];

class TableRoom extends React.Component {

    render() {
        const {id, name, temperature, lights, size, coordinates} = this.props.data;
        const {x, y} = coordinates;
        const {width, height} = size;
        const temperature_unit = this.props.data['temperature-unit'];
        /*const extended = this.props.extended;*/
        const filters = this.props.filters;

        let light_display = (<div> No lights to display </div>);
        if (lights.length !== 0) {
            if (true) {


                const light_elements = lights.map((light) => {
                        return (
                            <li key={'id-' + light.id} id={'id-' + light.id}>{light.id}</li>
                        )
                    }
                );
                light_display = (
                    <ul>
                        {light_elements}
                    </ul>
                )
            } else {
                const light_options = {
                    filters: {
                        active: true,
                        dimmable: true,
                        level: true,
                        type: true,
                        wattage: true,
                        kWh: true,
                        location: false,
                        id: true,
                    }
                };
                light_display = (<Table name='Lights' rows={lights} options={light_options}/>)
            }
        }

        const cells = [];

        if (filters.id === true) cells.push(<td>{id}</td>);
        if (filters.name === true) cells.push(<td>{name}</td>);
        if (filters.temperature === true) cells.push(<td>{temperature}&deg;{temperature_unit}</td>);
        if (filters.coordinates === true) cells.push(<td>({x}-{y})</td>);
        if (filters.size === true) cells.push(<td>{width * height}m<sup>2</sup> (w: {width}m, h: {height}m)</td>);
        if (filters.lights === true) cells.push(<td>{light_display}</td>);

        return (
            <tr key={id} id={id}>
                {cells}
            </tr>
        );
    }
}

class FloorPlanRoom extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false,
            over: false,
        }

    }

    hide_info = () => {
        setTimeout(() => {
            if (this.state.over === false) this.setState({open: false})
            else if (this.state.open === true) this.hide_info()
        }, 3000)

    }


    toggle_info = () => {
        debugger
        this.setState({open: !this.state.open});
        this.hide_info()
    }

    render() {
        const {coordinates, size, temperature, temperature_unit} = this.props.room;
        const {x, y} = coordinates;
        const {width, height} = size;
        const lights = this.props.lights;
        const nodes = this.props.nodes;


        const room_style = {
            top: y * 40,
            left: x * 40,
            width: width * 40, height: height * 40,
        };

        const temperature_index = Math.round((30 - parseInt(temperature)));
        const visibility = (this.props.temperature) ? 'visible' : 'hidden';

        const background_style = {
            visibility: visibility,
            backgroundColor: temperatures[temperature_index],
        };
        const light_display = (this.props.display_lights === true) ? lights.map(light => <FloorPlanLight
            light={light}/>) : (<div/>);
        const node_display = (this.props.display_nodes === true) ? nodes.map(node => <FloorPlanNode
            node={node}/>) : (<div/>);
        return (
            <div style={room_style} id={this.props.room.name + '_floorplan'} className='floor_plan_room'
                 onClick={() => this.toggle_info()}
                 onMouseOver={() => this.setState({over: true})}
                 onMouseOut={() => this.setState({over: false})}>
                <div className='title'> {this.props.room.name}</div>
                <div style={background_style} className='floor_plan_room_color'/>
                {!this.props.temperature ?
                    (<div className='room_temperature'>
                        {temperature}&deg;{temperature_unit}
                    </div>) : (<div/>)
                }
                <div className={'floor_plan_lights'}>
                    {light_display}
                </div>
                <div className={'floor_plan_lights'}>
                    {node_display}
                </div>
                <div className={(this.state.open) ? 'room_info open' : 'room_info closed'}>
                    <ul>
                        <li>Name: {this.props.room.name}</li>
                        <li>temp: {temperature}&deg;{temperature_unit} </li>
                        <li>Size: {width * height}m<sup>2</sup></li>
                        <li className='smaller'> (w: {width}m, h: {height}m)</li>
                    </ul>
                </div>
            </div>
        )
    }

}

export {
    TableRoom,
    FloorPlanRoom
};