import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import House from "./components/house.js";
import * as serviceWorker from './serviceWorker';
import BluePrint from "./components/blue_print";
import Menu from "./components/menu";
import Table from "./components/table_display";

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
