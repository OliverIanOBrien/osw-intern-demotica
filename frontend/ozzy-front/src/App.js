import React from 'react';
import logo from './images/Ozzy.png';
import icon from './images/Ozzy_small.png'
import './App.css';
import Header from "./components/header";
import Menu from "./components/menu";
import TableDisplay from "./components/table_display";
import BluePrint from "./components/blue_print";
import io from "socket.io-client";

const socket = io('')

class App extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            lights: {},
            rooms: {},
            nodes: {}
        }

    }

    componentDidMount() {
        fetch('/getHouse', {
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then(response => response.json())
            .then((data) => {
                //console.log(data);
                const {lights, rooms, nodes} = data;

                this.setState({
                    lights: lights,
                    rooms: rooms,
                    nodes: nodes,
                });
                //console.log(this.state);

            })
            .catch(error => {
                console.error(error)
            });

        socket.on('node-change', (data) => {
            debugger;
            let {nodes: new_value} = this.state;
            const {id, values} = data;
            const updated_values = Object.keys(values);
            updated_values.forEach(key => {
                new_value[id][key] = values[key];
            });
            this.setState({nodes: new_value})
        });

        socket.on('light-change', (data) => {
            debugger;
            let {lights: new_value} = this.state;
            const {id, values} = data;
            const update_values = Object.keys(values);
            update_values.forEach(key => {
                new_value[id][key] = values[key];
            })
            this.setState({lights: new_value});
        });

        socket.on('room-change', (data) => {
            let {rooms: new_value} = this.state;
            const {id, values} = data;
            const update_values = Object.keys(values);
            update_values.forEach(key => {
                new_value[id][key] = values[key];
            })
            this.setState({room: new_value});
        });

        socket.on('light-create', (data) => {
                let {lights: new_value} = this.state;
                const {id, values} = data;
                new_value[id] = values
                this.setState({lights: new_value})
            }
        );

        socket.on('node-create', (data) => {
                let {lights: new_value} = this.state;
                const {id, values} = data;
                new_value[id] = values
                this.setState({lights: new_value})
            }
        );

        socket.on('tick', (data) => {
            //console.log(data)
        })
    }

    componentWillUnmount() {
        socket.removeListener('data-change')
    }

    render() {
        return (
            <div className="App">
                <Header icon={icon} title='Home' id='home_header'/>
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <p>
                        Welcome to Ozzy front-end!
                    </p>
                </header>
                <Menu/>
                <TableDisplay state={this.state}/>
                <BluePrint state={this.state}/>
            </div>
        );
    }
}

export default App;
