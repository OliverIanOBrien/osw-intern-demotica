import hashlib
import json
from shutil import copymode, move
from tempfile import mkstemp

import speech_recognition as sr
import requests
from threading import Timer
import time
import argparse
import os, re

from playsound import playsound
from apscheduler.schedulers.background import BackgroundScheduler

ozzy_token = '854667e6-4258-4b80-b797-516e8886beaf'
ozzy_id = '5e67a7e09418d83ae25e7b6a'
headers = {'Authorization': 'TOK:854667e6-4258-4b80-b797-516e8886beaf'}
debugging = {
    'text to voice': 'T2V',
    'voice to text': 'V2T',
    'database storage': 'DB_IN',
    'connection oswald': 'OSW'
}
debug_bools = {
    'T2V': False,
    'V2T': False,
    'DB_IN': False,
    'OSW': False
}
debug = False


def to_voice(message, file_name):
    from gtts import gTTS
    gTTS(message, lang='en-AU').save(file_name)


def send_to_ozzy(message):
    data = {
        "message": message,
        "environment": "test",
        "session": "test",
        "locale": "en-gb"
    }
    return requests.post('https://api-acc.oswald.ai/api/v1/chats/5e67a7e09418d83ae25e7b6a/message', data, headers)


def talk_to_ozzy(input):
    reply = Ozzy_Message(send_to_ozzy(input).json())
    if debug_bools['OSW']: print(json.dumps([reply.reply, reply.quick_replies], indent=1))
    return reply


class Recorder:
    def __init__(self, note):
        self.recogniser = sr.Recognizer()
        self.recogniser.dynamic_energy_ratio = True
        self.microphone = sr.Microphone()
        self.timer = ResumableTimer(1.01, self.recalibrate_ambient())

    def pause_calibration(self):
        self.timer.pause()

    def resume_calibration(self):
        self.timer.resume()

    def recalibrate_ambient(self):
        with self.microphone as source:
            self.recogniser.adjust_for_ambient_noise(source)

    def record(self, show_all=True, limit=5):
        self.timer.pause()
        with self.microphone as source:
            audio = self.recogniser.listen(source, phrase_time_limit=limit)
        self.timer.resume()
        out = self.recogniser.recognize_google(audio, show_all=show_all)
        if debug_bools['V2T']: print(json.dumps(out, indent=2))
        return out


class Ozzy_Conversation:

    def __init__(self, recorder, voice):
        self.recorder = recorder
        self.voice = voice

    def check_if_contains(self, record, words):
        test = list(transcript['transcript'].lower() for transcript in
                    record['alternative']) if 'alternative' in record else record
        return any(any(word in el.lower() for el in test) for word in words)

    def test_options(self):
        voice.play_bot_byte('debug')
        record = self.recorder.record(True)
        alts = list(transcript['transcript'].lower() for transcript in
                    record['alternative']) if 'alternative' in record else record
        for key in debugging.keys():
            if any(key in el for el in alts):
                debug_bools[debugging[key]] = True

        print(debug_bools)

    def hello(self, words):
        record = self.recorder.record(True)
        out = self.check_if_contains(record, words) if record else False
        if out and self.check_if_contains(record, ['start debug']):
            self.test_options()
        if out and self.check_if_contains(record, ['stop debug']):
            for x in debug_bools.values(): x = False
        print(out)
        return out

    def talk(self):
        if debug_bools['OSW']:
            i = 0
            debug_command = ['Turn on the lights', 'give me the options', 'livingroom', 'options', 'no']
        reply = None
        attempts = 3
        if not debug_bools['OSW']:
            voice.play_bot_byte('on')
        while True:

            if debug_bools['OSW']:
                command = debug_command[i]
                i += 1
            else:
                command = self.recorder.record()
            if len(command) > 0:
                attempts = 3
                command = command['alternative'][0]['transcript']
                option_call = 'options' in command
                if 'you dance' in command:
                    msg = 'I can dance if I want to, I can leave my friends behind.' \
                            ' Cause if they dont dance. then they dont dance. and then they are no friends of mine'
                    message = {
                        'message':msg
                    }
                    data = {
                        'data': [message],
                        'quickReplies': []
                    }
                    reply = Ozzy_Message(data)
                elif 'turn yourself off' in command:
                    break
                else:
                    if not option_call:
                        reply = talk_to_ozzy(command)

                if reply:
                    if voice.speak(reply, option_call):
                        break
                else:
                    voice.play_bot_byte('error')
            else:
                attempts -= 1
                if attempts == 1:
                    voice.play_bot_byte('last')
                if attempts == 0:
                    break

        voice.play_bot_byte('off')


def split(group):
    reply = []
    for message in group:
        if re.search(r'(It is) (\w*[:,][ \w]+)', message):
            for m in re.match(r'(It is) (\w*[:,][ \w]+)', message).groups(): reply.append(m)
        elif re.search(r'\d+\.?\d*',message):
            for sentence in re.split(r'\.\s',message):
                reply.extend(re.match(r'(.*)\s(\d+\.?\d?)(.*)', sentence).groups())
        else:
            reply.append(message)
    return reply


class Ozzy_Message:

    def __init__(self, data):
        self.reply = split(map(lambda datum: datum['message'], data['data']))

        self.quick_replies = [datum['text'] for datum in data['quickReplies']] \
            if 'quickReplies' in data and data['quickReplies'] else []

    def __str__(self):
        return 'messages: {} \nquickreplies {}'.format(self.reply, self.quick_replies)

    def get_quick_replies(self):
        if len(self.quick_replies) == 0:
            return 'Currently no options to display.'
        return ', '.join(self.quick_replies)

    def get_reply(self):
        return '. '.join(self.reply)


class ResumableTimer:
    def __init__(self, timeout, callback):
        self.timeout = timeout
        self.callback = callback
        self.timer = Timer(timeout, callback)
        self.startTime = time.time()

    def start(self):
        self.timer.start()

    def pause(self):
        self.timer.cancel()
        self.pauseTime = time.time()

    def resume(self):
        self.timer = Timer(
            self.timeout - (self.pauseTime - self.startTime),
            self.callback)


class Voice:
    def __init__(self):
        self.memory = VoiceDB()

    def play_bot_byte(self, byte):
        self.play('Sound_bytes/Bot_sound_bytes/{}.mp3'.format(byte))

    def play(self, file):
        playsound(file)

    def speak(self, replies, quick_replies=False):
        if debug_bools['T2V']:
            breakpoint()
        end = False
        to_say = []
        if not quick_replies:
            for reply in replies.reply:
                start = time.time()
                if '###END###' in reply:
                    end = True
                    reply = reply.replace('###END###', '')
                if reply != '':
                    to_say.append(
                        self.memory.get_byte(reply) if self.memory.contains(reply) else self.memory.save_sound(reply))
                stop = time.time()
                print(str((stop - start) * 1000) + ' Milliseconds')
        else:
            to_voice(replies.get_quick_replies(), "temp.mp3")
            to_say.append("temp.mp3")
        for thing in to_say: self.play(thing)

        return end


class VoiceDB:

    def __init__(self):
        if not os.path.isdir('Sound_bytes/Bot_sound_bytes'): os.mkdir('Sound_bytes/Bot_sound_bytes')
        if not os.path.isdir('Sound_bytes/Reply_sound_bytes'): os.mkdir('Sound_bytes/Reply_sound_bytes')
        self.sound_bytes = {
            'base': {
                'I_am_sorry,_but_I_cannot_let_you_do_that': 'error.mp3',
                'Are_you_still_there?': 'last.mp3',
                'Shutting_off': 'off.mp3',
                'Yes?': 'on.mp3',
                'what_would_you_like_to_debug?': 'debug.mp3'
            },
            'dynamic': {}
        }
        self.reply_count = 0
        self.load_bot_bytes()
        self.changed_made = False
        self.backup = BackgroundScheduler()
        self.backup.add_job(self.update_backup, 'interval', seconds=10)
        self.backup.start()

    def contains(self, message):
        return message in self.sound_bytes['dynamic']

    def get_byte(self, message):
        self.changed_made = True
        self.sound_bytes['dynamic'][message]['count'] += 1
        return 'Sound_bytes/Reply_sound_bytes/{}'.format(self.sound_bytes['dynamic'][message]['file'])

    def update_backup(self):

        if self.changed_made:
            if debug_bools['DB_IN']:
                print('debugging')
            fh, abs_path = mkstemp()
            file_path = 'Sound_bytes/Reply_sound_bytes/reply_bytes.txt'
            replies = self.sound_bytes['dynamic'].items()
            reply_count = len(replies)
            with os.fdopen(fh, 'w') as new:
                new.write(str(reply_count) + '\n')
                for key, value in replies:
                    line = '{}||{}||{}\n'.format(key, value['file'], value['count'])
                    new.write(line)
                new.close()
            copymode(file_path, abs_path)
            os.remove(file_path)
            move(abs_path, file_path)
            self.changed_made = False
            print('backup made')

    def load_bot_bytes(self):
        print('loading bot sounds')

        for sentence, file in self.sound_bytes['base'].items():
            if not os.path.isfile('Sound_bytes/Bot_sound_bytes/' + file):
                print('{} -- missing'.format(sentence))
                to_voice(sentence.replace('_', ' '), 'Sound_bytes/Bot_sound_bytes/' + file)

        print('loading reply sounds')
        sounds = self.sound_bytes['dynamic']

        with open('Sound_bytes/Reply_sound_bytes/reply_bytes.txt', 'r') as replies:
            line = replies.readline()
            self.reply_count = int(line)
            line = replies.readline()
            line_number = 1

            while line:
                data = str(line)
                regex = re.compile('(.+[\w]*)\|\|(\w*.mp3)\|\|(\d+)').match(data)
                bytename = str(regex.group(1))
                file = regex.group(2)
                count = int(regex.group(3))
                byte = {
                    'count': int(count),
                    'file': file,
                }
                location = 'Sound_bytes/Reply_sound_bytes/{}'.format(file)
                sounds[bytename] = byte
                if not os.path.isfile(location): to_voice(bytename, location)
                line = replies.readline()
            replies.close()
        print('Loaded sounds')

    def save_sound(self, message):
        self.changed_made = True
        original = 'Sound_bytes/Reply_sound_bytes/reply_bytes.txt'
        dynamic = self.sound_bytes['dynamic']
        hash_obj = hashlib.sha1(message.encode())
        file_name = '{}{}'.format(hash_obj.hexdigest(), '.mp3')
        count = 1
        print('\'{}\' sound saved as {}'.format(message, file_name))
        to_voice(message, 'Sound_bytes/Reply_sound_bytes/{}'.format(file_name))
        dynamic[message] = {
            'count': count,
            'file': file_name
        }
        return 'Sound_bytes/Reply_sound_bytes/{}'.format(file_name)


if __name__ == "__main__":

    parse = argparse.ArgumentParser()
    parse.add_argument('-d', '--debug', action='store_true', default=False)
    options = parse.parse_args()
    debug = options.debug

    name = 'ozzy'
    hellos = [(word + ' ' + name).lower() for word in ['hey', 'hello', 'yo']]
    hellos.append(name)
    hellos.append('O C')
    hellos.append('Aussie')

    recorder = Recorder('note.mp3')
    voice = Voice()

    ozzy = Ozzy_Conversation(recorder, voice)

    playsound('note.mp3')

    while True:
        if debug:
            ozzy.talk()
            break
        else:
            if ozzy.hello(hellos):
                ozzy.talk()
