#!/bin/bash

echo 'Setting up Voice to Text'

echo 'installing prerequisite programs'

cd .

ABT=''

CHECK () {
  if ! type $1 > /dev/null; then
    return 0
  fi
  return 1
}

INSTALL () {
  if $(CHECK $1); then
    echo $1 'Not found: installing...'
      '$ABT install $1'
    echo 'Installed $1'
  else
    echo $1' Found'
  fi
}

if [[ "$OSTYPE" == "darwin"* ]]; then
  ABT='brew'
  echo 'MacOS detected'
  if "$(CHECK brew)" ; then
    echo 'Homebrew missing, instlling'
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
  else
    echo 'Homebrew installed, skipping install'
  fi
  brew list portaudio || brew install portaudio
  echo 'portaudio installed'
elif [[ "$OSTYPE" == "linux-gnu" ]]; then
    ABT='apt-get'
    $ABT install portaudio
fi

INSTALL python3
alias python=python3
python --version

INSTALL pip3
alias  pip=pip3
pip --version

INSTALL gcc
INSTALL g++

pip install -r requirements.txt

python voice2text.py

echo 'Done'